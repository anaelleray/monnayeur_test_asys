Fonctionnement :

Un monnayeur accepte plusieurs types de pièces de monnaie et fournit une date/heure limite de stationnement : 
Les pièces acceptées sont les suivantes : 20 centimes, 50 centimes, 1 euro, 2 euros.
Le tarif est dégressif et est le suivant : 1€ pour 20 minutes, 2€ pour 1h (60min), 5€ pour 12h (720min), 8€ pour 24h (1440min).
Le monnayeur ne rend pas la monnaie.

Exemples de résultat :

Si il est le 02/01/2017 11:10 lorsqu’on met 0€ :
- Si on met 3.50€, cela donne 02/01/2017 12:30 (3€ correspond à 2€ pour une heure + 1€ pour 20min), les 50 centimes ne servent à rien.
- Si on met 22 euros (que ce soit 11 pièces de 2€ ou 22 pièces de 1€ ou toute autre combinaison), cela donne 05/01/2017 1:30 (2 jours à 8€ + une demi-journée à 5€ + 1€ pour 20min)
