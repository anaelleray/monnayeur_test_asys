//INSTANCE WITHOUT FUNCTION ////////////////////////////
//Ces trois variables ci-dessous servent à obtenir le html qui sera édité avec les résultats, la somme ajoutée, l'heure d'arrivée et l'heure de départ.
let paid = document.getElementById('paid');
let arrival = document.getElementById('arrivalHtml');
let departure = document.getElementById('departureHtml');

//Ces deux variables sont les initialisations de la somme totale de temps obtenu et d'argent ajouté qui, au départ, sont à 0.
let timeTot = 0;
let moneyTot = 0;

//Ces deux lignes servent à ajouter la date de départ et l'afficher sur le html (date et heure précise)
let startDate = new Date(Date.now());
arrival.innerHTML = startDate.toLocaleDateString() + " à " + startDate.toLocaleTimeString();

///////////////////////////////////////////////////////

//ADD MONEY ///////////////////////////////////////////
//Cette fonction s'éxécute au clique sur les images de pièces, elle va vérifier l'id de l'image cliquée et en fonction de celui-ci on ajoute la valeur de l'argent à la valeur du total (moneyTot)
function addMoney() {

	if (event.target.id === "btn20") {
		moneyTot = moneyTot + 0.2

	} else if (event.target.id === "btn50") {
		moneyTot = moneyTot + 0.5

	} else if (event.target.id === "btn1") {
		moneyTot = moneyTot + 1

	} else if (event.target.id === "btn2") {
		moneyTot = moneyTot + 2
	}
	//Cette ligne sert à ajouter la valeur de l'argent obtenu au html
	paid.innerHTML = moneyTot;
}
///////////////////////////////////////////////////////

//CONVERT MONEY TO TIME AND SET THE DEPARTURE DATE/////
//Dans cette fonction exécutée au clique sur le bouton "Payer" on va déduire la somme de temps que l'on pourra obtenir. 
//"nb" récupère la valeur du total d'argent dans le html et à la boucle "while" signifie que tant que nb n'est pas égal à 0 mais reste suprérieur à 0.99 pour éviter les cas de nombre négatif ou les cas ou l'utilisateur ne mettrait que 0.2 ou 0.5$ on exécute ce qui suit. 
//L'éxécution est placée dans un ordre de grandeur en commençant par 8, tant que l'on peut soutraire 8 on le fait sinon on passe au valeurs plus petites. S'il reste une valeur en dessus de 1 elle n'est pas déduite.
function breakMoney() {
	let nb = paid.innerText;

	while (nb !== 0 && nb > 0.99) {
		if (nb - 8 >= 0) {
			nb = nb - 8
			timeTot = timeTot + 1440;

		} else if (nb - 5 >= 0) {
			nb = nb - 5
			timeTot = timeTot + 720;

		} else if (nb - 2 >= 0) {
			nb = nb - 2
			timeTot = timeTot + 60;

		} else if (nb - 1 >= 0) {
			nb = nb - 1
			timeTot = timeTot + 20;
		}
	}
	//Ces lignes permettent de remettre à 0 la valeur du html et la somme d'argent ajoutée une fois le paiement effectué.
	paid.innerText = 0;
	moneyTot = 0;

	//Dans cette fonction on va ajouter la date de départ, on récupère la date d'arrivée et on ajoute la somme de minutes obtenu (timeTot) par le calcul ci-dessus, puis on ajoute cette date au html au bon format.
	function departureDate() {
		let endDate = new Date(startDate.setMinutes(startDate.getMinutes() + (timeTot)));
		departure.innerHTML = endDate.toLocaleDateString() + " à " + endDate.toLocaleTimeString();
		timeTot = 0;

	} departureDate();
}